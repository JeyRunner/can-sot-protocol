#pragma once

#include "string"

static std::string genCodeTemplateHeaderContent = R"(
#pragma once

/**
 * THIS FILE IS AUTOGENERATED, DO NOT EDIT!
 * Generated by command '@@GENERATED_CMD@@'.
 */

#include <objectTree/OTNodeValueTypeDefs.h>
#include <objectTree/OTDeclares.h>
#include <objectTree/ProtocolDef.h>
#include "@@PROTOCOL_CLASS_NAME@@_Structs.hpp"

namespace @@PROTOCOL_CLASS_NAME@@Namespace {


/**
 * Protocol Definition for @@PROTOCOL_CLASS_NAME@@.
 * For the @@GENERATION_TARGET@@.
 */
template<typename COMC>
struct @@PROTOCOL_CLASS_NAME@@: public ProtocolDef<COMC, @@NODE_ID_TABLE_SIZE@@, @@NODES_TO_SEND_ON_INIT_TABLE_SIZE@@, @@RCCALLER_TABLE_SIZE@@, @@RCCALLABLE_TABLE_SIZE@@> {
  /// The object tree of the protocol
  struct ObjectTree: public Node {
@@OBJECT_TREE@@
  } objectTree;


  /// The remote calls of the protocol
  struct RemoteCalls {
@@REMOTE_CALLS@@
  } remoteCalls;


  /// the index is the node id belonging to the referenced node value
  OTNodeIDsTable<@@NODE_ID_TABLE_SIZE@@> otNodeIDsTable = {
@@NODE_ID_TABLE_CONTENT@@
  };


  /**
   * meta data objects that are send to master on communication initialization.
   * These are generally read only.
   */
  ValueNodeAbstract *metaNodeValuesToSendOnInit[@@NODES_TO_SEND_ON_INIT_TABLE_SIZE@@] = {
@@NODES_TO_SEND_ON_INIT_TABLE_CONTENT@@
  };


  /// Remote calls that can be called from this device
  /// the index is the call id belonging to the referenced remote call
  RemoteCallCallerAbstract *rcCallerTable[@@RCCALLER_TABLE_SIZE@@] = {
@@REMOTE_CALLS_CALLER_TABLE_CONTENT@@
  };

  /// Remote calls that are callable on this device. The caller will be another device.
  /// the index is the call id belonging to the referenced remote call
  RemoteCallCallableAbstract *rcCallableTable[@@RCCALLABLE_TABLE_SIZE@@] = {
@@REMOTE_CALLS_CALlABLE_TABLE_CONTENT@@
  };


  explicit @@PROTOCOL_CLASS_NAME@@(COMC *sotCanCommunication)
  : ProtocolDef<COMC, @@NODE_ID_TABLE_SIZE@@, @@NODES_TO_SEND_ON_INIT_TABLE_SIZE@@, @@RCCALLER_TABLE_SIZE@@, @@RCCALLABLE_TABLE_SIZE@@>(sotCanCommunication) {
    // setup all nodevalues
@@CONSTRUCTOR_SETUP_ALL_NODE_VALUES@@
  };
};


}
)";


static std::string genCodeTemplate_NodeWithChildren = R"(
struct @@NAME_TYPE@@: Node {
@@CONTENT@@
} @@NAME@@
)";

static std::string genCodeTemplate_ValueNode = R"(ValueNode@@READ_WRITABLE@@<@@TYPE@@, @@ID@@, COMC> @@NAME@@;
)";



static std::string genCodeTemplate_IdTableEntry_noComma = R"(valueNodeAsAbstract(@@NODE_PATH@@))";

static std::string genCodeTemplate_RemoteCallsTableEntry_Caller_noComma = R"(valueRemoteCallCallerAsAbstract(@@PATH@@))";
static std::string genCodeTemplate_RemoteCallsTableEntry_Callable_noComma = R"(valueRemoteCallCallableAsAbstract(@@PATH@@))";
static std::string genCodeTemplate_ConstructorSetupEntry = R"(@@NODE_PATH@@.__setProtocolRef(sotCanCommunication))";


static std::string genCodeTemplate_RemoteCallDataWritable = R"(
struct @@NAME_TYPE@@: RemoteCallDataWritable {
@@MEMBERS@@

  @@CONSTRUCTOR_WITH_ARGS@@
  @@NAME_TYPE@@() = default;

  const uint8_t getRequiredDataSizeBytes() final {
    return @@REQUIRED_SIZE@@;
  }

  /**
   * Write value to data of a can frame.
   */
  inline void _writeToData(uint8_t *data) final {
 @@DATA_CONVERSION@@
  }
};
)";

static std::string genCodeTemplate_RemoteCallDataReadable = R"(
struct @@NAME_TYPE@@: RemoteCallDataReadable {
@@MEMBERS@@

  @@CONSTRUCTOR_WITH_ARGS@@
  @@NAME_TYPE@@() = default;

  const uint8_t getRequiredDataSizeBytes() final {
    return @@REQUIRED_SIZE@@;
  }

  /**
   * Read value from data of a can frame.
   * This will overwrite the current value of the node.
   * This will also set the wasChangedEvent to true.
   */
  inline void _readFromData(const uint8_t *data) final {
 @@DATA_CONVERSION@@
  }
};
)";


static std::string genCodeTemplate_StructsHeader = R"(
#pragma once

/**
 * THIS FILE IS AUTOGENERATED, DO NOT EDIT!
 * Generated by command '@@GENERATED_CMD@@'.
 * For the @@GENERATION_TARGET@@.
 */

#include "objectTree/OTNodeValueTypeDefs.h"
#include "objectTree/OTDeclares.h"
#include "objectTree/ProtocolDef.h"
#include "remoteCalls/RemoteCalls.h"

namespace @@PROTOCOL_CLASS_NAME@@Namespace {

@@ENUM_DEFS@@


@@REMOTE_CALLS_DATA_STRUCTS@@

}
)";





static string insertIntoTemplate(string templateStr, map<string, string> replacementValues) {
    for (auto [key, value] : replacementValues) {
        string keyFull = "@@" + key + "@@";
        bool replaced_once = false;
        auto loc = templateStr.find(keyFull);
        while (loc != std::string::npos) {
            templateStr.replace(loc, keyFull.size(), value);
            //cout << "replaced " << keyFull << endl;
            replaced_once = true;
            loc = templateStr.find(keyFull, loc);
        }
        if (!replaced_once) {
            cout << "could not replace key '" << keyFull << "' (not found) in template: " << templateStr << endl;
            throw runtime_error("insertIntoTemplate error");
        }

    }
    // check if all replaced
    auto loc = templateStr.find("@@");
    if (loc != std::string::npos) {
        auto endLoc = templateStr.find("@@", loc+2);
        string key = templateStr.substr(loc, endLoc-loc+2);
        cout << "not all keys were replaced in template, key '"<< key <<"' was not replaced in template: " << templateStr << endl;
        throw runtime_error("insertIntoTemplate error");
    }
    return templateStr;
}