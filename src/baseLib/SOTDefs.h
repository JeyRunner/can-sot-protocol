#pragma once

constexpr bool IS_MASTER = true;
constexpr bool IS_CLIENT = true;


#define IS_MASTER_DEF
#define IS_CLIENT_DEF