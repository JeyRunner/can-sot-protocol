## General Package Structure
The 11 bit CAN ID field and Data of a CAN frame is split into:

| 0-2 bit of CAN ID | 3-5 bit of CAN ID     | 6-11 bit of CAN ID     | 0-8 Bytes of Data |
|-------------------|-----------------------|------------------------|-------------------|
| Source Device  ID | Destination Device ID | SOT Message ID (5 bit) | ...               |

For the `Device ID` (3 bits), ID 0 is reserved to the master, all other IDs (1-7) are for the clients. The first 6 bit of the can frame contain the `Destination Device ID` for the device this frame is addressed to and the `Source Device ID` indicating from which device this frame is coming from.
The `SOT Message ID` determines the type of the message/package.


CAN Package Direction:

- 🖥️ ➡️ 🎚️️ &ensp; master/server sends to client
- 🎚️ ➡️ 🖥 &ensp; clients sends to master/server
- 🖥️ 🔄 🎚️️ &ensp; both client of master/server can send package to the other part

## General Communication Flow
This is an example for a typical communication flow. <br/>
Here the client offers a `tmp_request_value` called `mode.requestMode` to activate streaming of some sensor data (there is code on the  client to do that). This will change the clients `currentStatus` value.

```mermaid
sequenceDiagram
    Master->>Client: Init Communication Request
    Note right of Client: Clients sends its <br/> _meta data information to server:
    Client->>Master: Write Node Value Request ("_meta.protocolVersion")
    Client->>Master: Write Node Value Request ("_meta.endianness")
    Client->>Master: Init Communication Response

    Note over Master,Client: _<br/> Now master will write all setting node values on client:
    Master->>Client: Write Node Value Request (e.g. "settings.value1")
    Master->>Client: Write Node Value Request (e.g. "settings.value2")

    Note over Master,Client: _<br/> Now a 'tmp_request_value' may be used <br/> to activate some mode on the client:
    Master->>Client: Write Node Value Request ("mode.requestMode": MODE_RUNNING)
    Client->>Master: Write Node Request Value Response (OK)
    Client->>Master: Write Node Value Request ("currentStatus": STATUS_RUNNING)
    Note over Master,Client: Now the clients start streaming some sensor data:
    Client->>Master: Stream Package (SensorDataSP)
    Note over Master,Client: ...
```

## Control Packages

#### Init Communication Request  &ensp; 🖥️ ➡️ 🎚️
This is the first exchanged message (from master to client). <br/>
After receiving this message, the client will first send all its `_meta` information of the OT to the master (via Write Node Value Requests).
After the client is finished with sending all values in `_meta`, it will respond with an Init Communication Response.

| SOT Message ID (5 bit) | Data 0 byte |
|------------------------|-------------|
| 0b00'0000              |             |

#### Init Communication Response  &ensp; 🎚️ ➡️ 🖥
This is the response of the client to the masters Init Communication Request (from client to master). 
Afterward, client and master are connected.

| SOT Message ID (5 bit) | Data 1 byte                                                                  |
|------------------------|------------------------------------------------------------------------------|
| 0b00'0001              | `0` if communication is accepted.<br/> `1` if communication is not accepted. |


#### Disconnect Communication Request  &ensp; 🖥️ ➡️ 🎚️
To disconnect a client, the master can send a disconnect communication request to the client.
Afterward, client and master are not connected anymore.

| SOT Message ID (5 bit) | Data 0 byte |
|------------------------|-------------|
| 0b00'0011              |             |

#### Communication Error  &ensp; 🖥️ 🔄 🎚
On device sends this to the other device its communicating with to indicate there is a general communication error/problem.
Error regarding buffer overflows are currently only send form client to server.

| SOT Message ID (5 bit) | Data 1 byte                                                                                        |
|------------------------|----------------------------------------------------------------------------------------------------|
| 0b00'0010              | `0` Receive Buffer Overflow (other sending device should slow down).<br/> `1` Send Buffer Overflow |



## Object Directory Read / Write
Works in both directions from master to client and the other way.

### Write

#### Write Node Value Request  &ensp; 🖥️ 🔄 🎚️️

| SOT Message ID (5 bit) | Data 1 byte    | Data 1-7 bytes                                |
|------------------------|----------------|-----------------------------------------------|
| 0b00'0100              | Object Node ID | Object Node Value (size depends on data type) |

#### Write Node Value Ack *  &ensp; 🖥️ 🔄 🎚️️

Acknowledge is sent after Write Node Value Request was received (Note, that this is currently not implemented).

| SOT Message ID (5 bit) | Data 1 byte    |
|------------------------|----------------|
| 0b00'0101              | Object Node ID |

#### ~~Write Node Request Value Response  &ensp; 🖥️ 🔄 🎚️️~~

~~When a node value was written (via Write Node Value Request) that is declared as `is_tmp_request_value: true`, this response message is send back by node that received the write request.~~

| ~~SOT Message ID (5 bit)~~ | Data 1 byte    | Data 1 byte                                                                |
|------------------------|----------------|----------------------------------------------------------------------------|
| ~~0b00'0110~~             | Object Node ID | `0` if write was accepted by receiver.<br/> `1` if write was not accepted. |

### Read

#### Read Node Value Request  &ensp; 🖥️ 🔄 🎚️️

| SOT Message ID (5 bit) | Data 1 byte    |
|------------------------|----------------|
| 0b00'1000              | Object Node ID |

#### Read Node Value Response  &ensp; 🖥️ 🔄 🎚️️

Response is sent after Write Node Value Request was received.

| SOT Message ID (5 bit) | Data 1 byte    | Data 1-7 bytes                                |
|------------------------|----------------|-----------------------------------------------|
| 0b00'1001              | Object Node ID | Object Node Value (size depends on data type) |

## Stream Packages

#### Stream Package  &ensp; 🖥️ 🔄 🎚️️

| SOT Message ID (5 bit)        | Data 0-8 bytes                                |
|-------------------------------|-----------------------------------------------|
| Variable SP ID (value: 16-32) | Object Node Value (size depends on data type) |



## Remote Calls

#### Remote Call Request  &ensp; 🖥️ ➡️ 🎚️️

| SOT Message ID (5 bit) | Data 1 byte                       | Data 0-7 bytes          |
|------------------------|-----------------------------------|-------------------------|
| 0b00'1100              | Remote call ID (0-6bit) (max 126) | Argument data (chained) |


#### Remote Call Return  &ensp; 🎚️ ➡️ 🖥
The `Remote Call` will allways lead to `Remote Call Return` back to the caller.

| SOT Message ID (5 bit) | Data 1 byte                                                     | Data 0-7 bytes                              |
|------------------------|-----------------------------------------------------------------|---------------------------------------------|
| 0b00'1101              | Remote call ID (0-6bit), <br/>return ok (7.bit) (ok when 7.bit == 1) | Return data (chained), or 1 byte error code |